import React, { Component } from 'react';
import { transactionAPI, searchAPI, loginAPI, connectAPI } from '../../utils/apiService';
import './home.css';
import { getCookie, setCookie } from '../../utils/cookieService';

import { AutoComplete, InputNumber, Radio, Button} from 'antd';


function login(id) {
	// Delete Old User Data
	setCookie('userID', undefined, -1)
	setCookie('userData', undefined, -1)
    // Call Login API to get userData
	return loginAPI(id)
		.then(({connected, smallcaseAuthToken: userData}) => {
			id && setCookie('userID', id, 7)
			userData && setCookie('userData', userData, 7)
			return {connected, userData};
		})

}

export default class home extends Component {  

    constructor(props){
        super(props);
        this.state = {
            status: '',
            email : '',
            stock: '',
            quantity: 1,
            type: 'BUY',
            stocks: [],
            stockResult: [],
        }

    }

    updateStatus(status){
        this.setState({
            status
        });
    }

    handleEmailUpdate(email){
        this.setState({
            email
        });
    }


    initiateConnection = () =>{
        console.log('🏁 Initiating Connection');
        // Talking to smartinvesting backend to create a transaction (transactionID) by talking to gateway backend
        const { email } = this.state;
        if (this.state.email !== '') {
            login(email, this.updateStatus)
                .then(({connected, userData}) => {
                    if (connected) {
                        return window.sc.init({ userData })
                    }
                    return transactionAPI(email, 'CONNECT')
                        .then(({ transactionId }) =>{
                            return window.sc.triggerTransaction({
                                transactionId,
                            })
                        }
                        )
                        .then(({smallcaseAuthToken}) => connectAPI(email, smallcaseAuthToken))
                        .then(() => login(email, this.updateStatus))
                })
        }
    }  

    handleStockName = (stock) => {
        this.setState({
          stock,
        })
      }

    handleSearch = (value) => {
        this.setState({
          stock: value,
        }, () => {
          if (value && value.length >= 3) {
            searchAPI(value)
              .then(res => res.results)
              .then(stocks => (stocks.map(stock => stock.stock.info.ticker)))
              .then(stockResult => this.setState({
                stockResult,
              }));
          }
        })
    }

    triggerOrder = () => {
        if (this.state.stocks.length > 0) {
          transactionAPI(getCookie('userID'), 'TRANSACTION', {
            type: 'SECURITIES',
            securities: this.state.stocks,
          })
            .then(({ transactionId }) => {
              this.setState({
                transactionId,
              });
              return transactionId;
            })
            .then(transactionId => window.sc.triggerTransaction({
              transactionId,
            }))
            .then(res => {
                console.log('Order placed successfully',  res);
                alert(`Order placed ! TransactionID: ${res.transactionId}`)
              const [lastBatch] = res.orderBatches;
              const orders = lastBatch.orders
              this.setState({
                visible: true,
                orders: orders.map(order => ({ tradingSymbol: order.tradingsymbol, status: order.status }))
              })
            })
            .catch(err => {
              if (err.message === 'invalid_user_data') {
                
              }
              throw err;
            })
        }
      }
    


    addStock = () => {
        if (this.state.stock !== "") {
            this.setState(state => ({
            stocks: [
                ...state.stocks,
                {
                ticker: state.stock,
                type: state.type,
                quantity: state.quantity,
                }
            ],
            stock: "",
            stockResult: [],
            quantity: 1
            }))
        }
    }

    handleQuantityChange = (quantity) => {
        this.setState({
            quantity,
        })
        }

    removeStock = (ticker) =>{
        this.setState(state => ({
            stocks: state.stocks.filter(stock => stock.ticker !== ticker)
          }));
    }


    getEverythingInCart(){
        if(this.state.stocks.length > 0){
            return this.state.stocks.map( stock =>(
                <div className='stockCard'>
                    <div>
                        {stock.ticker}
                    </div>
                    <div>
                        {stock.quantity}
                    </div>
                    <div>
                        {stock.type}
                    </div>
                    <div className='RemoveBtn' onClick={()=>this.removeStock(stock.ticker)}>🗑 Remove</div>
                </div>
            ));
        }
        return (
            <div>
                bucket is empty 😕
            </div>
        )
    }

    handleTypeChange(type){
        this.setState({
            type
        })
    }

    render() {
        const { stockResult, stock, type } = this.state;
        return (
            <div>
                <div className='mainContainer'>
                    <div className='loginContainer'>
                        <input className='email' type='text' onChange={e => this.handleEmailUpdate(e.target.value.trim())} placeholder="email or username" />
                        <div className='btn' onClick={this.initiateConnection}>Get connected</div>
                    </div>

                    <div className='searchBoxContainer'>

                        <div className='inputsContainer'>
                            <AutoComplete
                                dataSource={stockResult}
                                onSelect={this.handleStockName}
                                onSearch={this.handleSearch}
                                placeholder="Search Stocks"
                                className="mr8 auto-complete"
                                value={stock}
                            />
                            
                            <InputNumber className='qtyInput' min={1} defaultValue={1} onChange={this.handleQuantityChange} />
                        </div>

                        <Radio.Group defaultValue="BUY" buttonStyle="solid" onChange={(e) => this.handleTypeChange(e.target.value)}>
                            <Radio.Button value="BUY">Buy</Radio.Button>
                            <Radio.Button value="SELL">Sell</Radio.Button>
                        </Radio.Group>

                        <div className='Add_btn' onClick={this.addStock}>Add</div>
                        
                    </div>

                    <div className='placeOrderBtn' onClick={this.triggerOrder}>Place Order</div>
                </div>
                <div className='bottom_container'>
                    <div className='cart'>
                        {this.getEverythingInCart()}

                    </div>
                </div>
            </div>
        )
    }
}
