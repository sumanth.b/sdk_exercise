export function saveGatewayUserData(payload){
    localStorage.setItem("gatewayUserData", JSON.stringify(payload));
}

export function getGatewayUserData(payload){
    const item = JSON.parse(localStorage.getItem("gatewayUserData"));
    console.log('gateway user data is', item);
    return item;
}