import axios from 'axios'

const httpHandler = axios.create({
   baseURL: 'https://api.dev.smartinvesting.io',
   headers: {
      'Content-Type': 'application/json',
   }
})

export function searchAPI(text) {
   return httpHandler.get('/search', {
      params: {
         text,
      }
   })
   .then(res => res.data)
}

export function loginAPI(id) {
   return httpHandler.post('/user/login', {
      id, 
   })
   .then(res => res.data)
}

export function connectAPI(id, smallcaseAuthToken) {
   return httpHandler.post('/user/connect', {
      id, 
      smallcaseAuthToken,
   })
}


export function transactionAPI(id, intent, orderConfig) {
   console.log('id is', id);
   return httpHandler.post('/transaction/new', {
      id,
      intent,
      orderConfig,
   })
   .then(res => res.data)
}

export function holdingsAPI(id) {
   return httpHandler.get('/holdings/fetch', {
      params: {
         id,
      }
   })
   .then(res => res.data)
}


export function postbackAPI(transactionId) {
   return httpHandler.get('/transaction/response', {
      params: {
         transactionId,
      },
   })
   .then(res => res.data)
}