import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { getCookie } from './utils/cookieService';
import App from './App';

// Initializing the scDK by checking if userdata cookie exists or else use guest JWT 
const userData = getCookie('userData') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJndWVzdCI6dHJ1ZSwiaWF0IjoxNTU4NjgzMTM5LCJleHAiOjE4Njk3MjMxMzl9.Xc-h4WEu6s_6mQ28sqUaT49ARCpNrIL6KBKdiiGryZs';
window.sc =  new window.scDK({
	gateway: 'gatewaydemo',
	userData,
})

ReactDOM.render(<App />, document.getElementById('root'));